import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../servicios/heroes.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {
  

  hay: boolean;
  heroesFound: any;
  
  constructor(private activatedRouted:ActivatedRoute, private heroesservice:HeroesService) { 

    this.activatedRouted.params.subscribe(params => {
      console.log("Parametro recibido: "+params['term']);
      this.heroesFound = this.heroesservice.buscarHeroes(params['term']);
     //  console.log(this.heroesFound);
    })
  //  this.activatedRouted.params.subscribe(params => {
  //    console.log("Parametro recibido: "+params['id']);
  //    this.heroe = this.heroesservice.getHeroe(params['id']);
  //    console.log(this.heroe);
  }

  ngOnInit() {
  }

  mostrarHeroes(termino: string){
   

  }


}
