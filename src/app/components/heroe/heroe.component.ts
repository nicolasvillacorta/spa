import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../servicios/heroes.service'

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

  heroe:any;
  hay = false;

  constructor(private activatedRouted:ActivatedRoute, private heroesservice:HeroesService) { 

    this.activatedRouted.params.subscribe(params => {
      console.log("Parametro recibido: "+params['id']);
    //  this.heroe = this.heroesservice.getHeroe(params['id']);
    //  console.log(this.heroe);
      this.heroesservice.getHeroeAPI(params['id'])
      .subscribe(data => {
        console.log("Imprimo vuelta de heroe api")
        console.log(data)
        console.log(data.results);
        this.heroe = data.results;
        this.hay = true;
      },
         error => {
           console.log("fallo el call de la API");
         
           console.log(error)
         });

         console.log(this.heroe);
    })

  }

  ngOnInit() {
  }

}
