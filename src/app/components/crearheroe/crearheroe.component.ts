import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HeroesService } from '../servicios/heroes.service'

@Component({
  selector: 'app-crearheroe',
  templateUrl: './crearheroe.component.html',
  styleUrls: ['./crearheroe.component.css']
})
export class CrearheroeComponent implements OnInit {

  creoHeroe = {
    nombre: '',
    bio: '' ,
    img: '',
    aparicion: '',
    casa:''
   }

  constructor(private heroesService: HeroesService) { }

  ngOnInit() {
  }

  guardar(forma:NgForm) {
    // console.log("guardo los cambios");
    // console.log("NgForm forma: ", forma);
    // console.log("forma value: ", forma.value);
    console.log("creoHeroe", this.creoHeroe);

    this.heroesService.postHeroesAPI(this.creoHeroe)
    .subscribe(data => {
      console.log(data)
       },
      error => {
         console.log("fallo el call de la API");
       
         console.log(error)
       });
}



}
