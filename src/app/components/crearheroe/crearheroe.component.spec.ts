import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearheroeComponent } from './crearheroe.component';

describe('CrearheroeComponent', () => {
  let component: CrearheroeComponent;
  let fixture: ComponentFixture<CrearheroeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearheroeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearheroeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
